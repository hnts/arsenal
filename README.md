# arsenal
## Roadmap
- [x] railsgoat on k8s
- [ ] Gitlab Runner on k8s
- [ ] Install Gitlab
- [ ] Vulnerability management
- [ ] CI/CD
  - [ ] security automation
    - [ ] sast
    - [ ] dast
  - [ ] test automation